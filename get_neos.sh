#!/usr/bin/env bash

wget https://s3.amazonaws.com/qcdneos/neosB-v0.1.tar.gz
wget https://s3.amazonaws.com/qcdneos/neosBS-v0.1.tar.gz
wget https://s3.amazonaws.com/qcdneos/neosBQS-v0.1.tar.gz
