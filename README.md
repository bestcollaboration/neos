## NEOS

This is a repository for downloading an equation of state of the QCD matter at finite baryon density, NEOS.

NEOS is a QCD equation of state model at finite chemical potentials including net baryon, electric charge, and strangeness, based on the conserved charge susceptibilities determined from lattice QCD simulations and the equation of state of the hadron resonance gas model. It is aimed for the applications to relativistic hydrodynamic models of nuclear collisions at wide range of collision energies.

Please visit the official [website](https://sites.google.com/view/qcdneos) for details.

Reference: A. Monnai, B. Schenke, C. Shen, [arXiv:1902.05095 [nucl-th]](https://arxiv.org/abs/1902.05095)
